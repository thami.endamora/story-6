$(document).ready(function () {
    $('#buttonPINK').click(function () {
        $('nav').css('background-color', '#ffbfdd');
        $('.jumbotron').css('background-color', '#ffbfdd');
        $('.accordionTitle').css('color', '#ffbfdd');
        $('.accordionContent').css('color', '#ffbfdd');
    });

    $('#buttonYELLOW').click(function () {
        $('nav').css('background-color', '#ffbc42');
        $('.jumbotron').css('background-color', '#ffbc42');
        $('.accordionTitle').css('color', '#ffbc42');
        $('.accordionContent').css('color', '#ffbc42');
    });

    $(function () {
        $("#accordion").accordion({
            collapsible: true,
		    active: false,
            heightStyle: "content",
        });
    });
});