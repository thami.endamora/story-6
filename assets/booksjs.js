$(document).ready(function () {
    $('#buttonSubmit').click(function () {
        var query = $('#search_input').val();
        $(function () {
            $.ajax({
                url: "/books/readJSON/" + query,
                success: function (result) {
                    $('#table-books').empty();
                    $.each(result.items, function (i, item) {
                        $('<tr>').append(
                            $('<td>').append('<img style="max-height: 150px;max-width: 150px" src=' + item.volumeInfo.imageLinks.thumbnail + "'/>"),
                            $('<td>').text(item.volumeInfo.title),
                            $('<td>').text(item.volumeInfo.authors),
                            $('<td>').text(item.volumeInfo.publisher),
                            $('<td>').append('<a href="' + item.volumeInfo.canonicalVolumeLink + '" target="_blank">LINK</a>')
                        ).appendTo('#table-books');
                    });
                }
            });
        });
    });
    $('#search_input').val("example");
    $('#buttonSubmit').click();
});