from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from . import views

class StatusUnitTest(TestCase):
    #     GENERAL TESTS     #
    def test_about_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, views.about)

    def test_about_using_about_template(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

class StatusFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    #     TESTS FOR JAVASCRIPT     #
    def test_change_theme_pink(self):
        selenium = self.selenium
        selenium.get('http://ppw6-thamiendamora.herokuapp.com/about')
        time.sleep(5)
        ganti = selenium.find_element_by_id('buttonPINK')
        ganti.send_keys(Keys.RETURN)
        time.sleep(5)
        newbackground = selenium.find_element_by_class_name('jumbotron').value_of_css_property('background-color')
        self.assertEqual(newbackground, "rgba(255, 191, 221, 1)")
        newtitle = selenium.find_element_by_class_name('accordionTitle').value_of_css_property('color')
        self.assertEqual(newtitle, "rgba(255, 191, 221, 1)")
        newcontent = selenium.find_element_by_class_name('accordionContent').value_of_css_property('color')
        self.assertEqual(newcontent, "rgba(255, 191, 221, 1)")

    def test_change_theme_yellow(self):
        selenium = self.selenium
        selenium.get('http://ppw6-thamiendamora.herokuapp.com/about')
        time.sleep(5)
        ganti = selenium.find_element_by_id('buttonYELLOW')
        ganti.send_keys(Keys.RETURN)
        time.sleep(5)
        newbackground = selenium.find_element_by_class_name('jumbotron').value_of_css_property('background-color')
        self.assertEqual(newbackground, "rgba(255, 188, 66, 1)")
        newtitle = selenium.find_element_by_class_name('accordionTitle').value_of_css_property('color')
        self.assertEqual(newtitle, "rgba(255, 188, 66, 1)")
        newcontent = selenium.find_element_by_class_name('accordionContent').value_of_css_property('color')
        self.assertEqual(newcontent, "rgba(255, 188, 66, 1)")