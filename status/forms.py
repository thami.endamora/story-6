from django import forms
from .models import StatusBar
from django.forms import ModelForm

class StatusForm(ModelForm):
    class Meta:
        model = StatusBar
        fields = ['statustext']
        labels = {'statustext':'Status'}
        widgets = {
            'statustext': forms.TextInput(attrs={'class': 'form-control form-control-sm',
                                               'type': 'text',
                                               'placeholder': 'Masukkan status kamu disini'})
        }
