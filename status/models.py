from django.db import models

class StatusBar(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    statustext = models.TextField()