from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import StatusBar

def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            status = form.cleaned_data['statustext']
            if (len(status) < 301): #Maximum length: 300
                form.save()
                return redirect('/')
            else:
                return redirect('/')
    else:
        form = StatusForm()

    allstatus = StatusBar.objects.all()
    content = {'title':'Status',
               'allstatus':allstatus,
               'form':form}
    return render(request, 'index.html',content)