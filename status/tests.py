from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

from . import views
from .models import StatusBar
from .forms import StatusForm

class StatusUnitTest(TestCase):
    #     GENERAL TESTS     #
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('/wek/')
        self.assertEqual(response.status_code, 404)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    #     TESTS FOR MODELS     #
    def test_model_can_create_new_status(self):
        new_status = StatusBar.objects.create(statustext='Sedang mengerjakan Story 6')
        counting_all_available_status = StatusBar.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    #     TESTS FOR FORMS     #
    def test_post_success_and_render_the_result(self):
        text = 'Sedang belajar PPW'
        response = Client().post('/', {'statustext': 'Sedang belajar PPW'})
        self.assertEqual(response.status_code, 302) # Redirect
        html_response = response.content.decode('utf8')
        self.assertIn(html_response,text)

    def test_forms_valid(self):
        form = StatusForm(data={'statustext':'Coba Coba'})
        self.assertTrue(form.is_valid())

    def test_forms_not_valid(self):
        coba301 = 'Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba'
        response = Client().post('/', {'statustext': coba301})
        html_response = response.content.decode('utf8')
        self.assertNotIn(coba301, html_response)

class StatusFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(StatusFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusFunctionalTest, self).tearDown()

    def test_input_valid(self):
        selenium = self.selenium
        selenium.get('http://ppw6-thamiendamora.herokuapp.com/')
        time.sleep(5)
        status = selenium.find_element_by_id('id_statustext')
        submit = selenium.find_element_by_id('buttonSubmit')
        status.send_keys('Coba Coba')
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
        self.assertIn("Coba Coba", selenium.page_source)

    def test_input_not_valid(self):
        selenium = self.selenium
        selenium.get('http://ppw6-thamiendamora.herokuapp.com/')
        time.sleep(5)
        status = selenium.find_element_by_id('id_statustext')
        submit = selenium.find_element_by_id('buttonSubmit')
        coba301 = 'Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba Coba'
        status.send_keys(coba301)
        submit.send_keys(Keys.RETURN)
        time.sleep(5)
        self.assertNotIn(coba301,selenium.page_source)

# Terima kasih kepada Bu Iis & Bu Naya, Gitlab PPW2017, Kak Nio, Kak Upi, teman - teman semua yang membantu <3 #