from django.test import TestCase, Client
from django.urls import resolve
from . import views

class BlogUnitTest(TestCase):
    #     GENERAL TESTS     #
    def test_blog_is_exist(self):
        response = Client().get('/blog/')
        self.assertEqual(response.status_code, 200)

    def test_blog_using_blog_func(self):
        found = resolve('/blog/')
        self.assertEqual(found.func, views.blog)

    def test_blog_using_blog_template(self):
        response = Client().get('/blog/')
        self.assertTemplateUsed(response, 'blog.html')