from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import UserForm

def userLogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user) # Saves the user’s ID in the session using Django’s session framework
            return redirect('/')
        else:
            return render(request, 'login.html', {'failed':True})
    else:
        return render(request, 'login.html', {'failed':False})

def userRegister(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            registered = True
    else:
        user_form = UserForm()
    return render(request, 'register.html', {'user_form': user_form, 'registered': registered})

@login_required
def userLogout(request):
    logout(request) # Session data will be completely cleaned out
    return redirect('/')
