from django.urls import path
from . import views

urlpatterns = [
    path('login/',views.userLogin,name='userlogin'),
    path('register/',views.userRegister,name='userregister'),
    path('logout/',views.userLogout,name='userlogout'),
]