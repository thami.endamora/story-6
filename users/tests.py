from django.contrib import auth
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
from . import views
from .forms import UserForm

class UsersUnitTest(TestCase):
    #     GENERAL TESTS FOR LOGIN PAGE    #
    def test_login_is_exist(self):
        response = Client().get('/user/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_using_login_func(self):
        foundlogin = resolve('/user/login/')
        self.assertEqual(foundlogin.func, views.userLogin)

    def test_login_using_login_template(self):
        response = Client().get('/user/login/')
        self.assertTemplateUsed(response, 'login.html')

    #     GENERAL TESTS FOR REGISTER PAGE    #
    def test_register_is_exist(self):
        response = Client().get('/user/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_register_func(self):
        foundregister = resolve('/user/register/')
        self.assertEqual(foundregister.func, views.userRegister)

    def test_register_using_register_template(self):
        response = Client().get('/user/register/')
        self.assertTemplateUsed(response, 'register.html')

    #     GENERAL TESTS FOR LOGOUT   #
    def test_logout_is_exist(self):
        response = Client().get('/user/logout/')
        self.assertEqual(response.status_code, 302) #Redirect

    def test_logout_using_logout_func(self):
        foundlogout = resolve('/user/logout/')
        self.assertEqual(foundlogout.func, views.userLogout)

    #     TESTS FOR REGISTER/LOGIN/LOGOUT   #
    def setUp(self) :
        self.client = Client()
        new_user = User.objects.create_user(username='cobauser', email='coba@gmail.com', password='coba123')

    def test_user_register(self):
        registeruser = self.client.post('/user/register/', {'username': 'cobauser','email':'coba@gmail.com','password':'coba123'})
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)

    def test_user_can_login(self):
        response = self.client.post('/user/login/', {'username': 'cobauser', 'password': 'coba123'})
        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)
        text = 'WELCOME, cobauser!'
        html_response = response.content.decode('utf8')
        self.assertIn(html_response,text)

    def test_user_cannot_login(self):
        response = self.client.post('/user/login/', {'username': 'coba!!!', 'password': 'coba123'})
        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated)
        text = 'WELCOME, cobauser!'
        html_response = response.content.decode('utf8')
        self.assertNotIn(html_response, text)

    def test_user_can_logout(self):
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('LOGOUT', html_response)
        self.client.login(username='cobauser',password='coba123')
        response = self.client.get('/user/logout/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('LOGOUT', html_response)

class UserFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(UserFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(UserFunctionalTest, self).tearDown()

    def test_user(self):
        selenium = self.selenium
        # REGISTER
        selenium.get('http://127.0.0.1:8000/user/register/')
        time.sleep(3)
        username = selenium.find_element_by_id('id_username')
        password = selenium.find_element_by_id('id_password')
        email = selenium.find_element_by_id('id_email')
        register = selenium.find_element_by_id('buttonUSER')
        username.send_keys('cobauser')
        email.send_keys('coba@gmail.com')
        password.send_keys('coba123')
        register.send_keys(Keys.RETURN)
        time.sleep(3)

        # LOGIN
        selenium.get('http://127.0.0.1:8000/user/login/')
        time.sleep(3)
        loginUsername = selenium.find_element_by_name('username')
        loginPassword = selenium.find_element_by_name('password')
        login = selenium.find_element_by_id('buttonUSER')
        loginUsername.send_keys('cobauser')
        loginPassword.send_keys('coba123')
        login.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('WELCOME, cobauser!', selenium.page_source)