from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

response = {}
googlebooks = 'https://www.googleapis.com/books/v1/volumes?q='

def readJSON(request,query):
    url = googlebooks + query
    json_data = json.loads(requests.get(url).text)
    return JsonResponse(json_data)

def indexbooks(request):
    return render(request, 'books.html',response)