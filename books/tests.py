from django.test import TestCase, Client
from django.urls import resolve
from . import views

class BooksUnitTest(TestCase):
    #     GENERAL TESTS     #
    def test_books_is_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_books_using_books_func(self):
        foundbooks = resolve('/books/')
        self.assertEqual(foundbooks.func, views.indexbooks)

    def test_books_using_books_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'books.html')

    #     TEST FOR JSON     #
    def test_JSON_page_url_is_exist(self):
        response = Client().get('/books/readJSON/quilting/')
        self.assertEqual(response.status_code, 200)

    def test_JSON_using_readJSON_func(self):
        foundjson = resolve('/books/readJSON/quilting/')
        self.assertEqual(foundjson.func, views.readJSON)

    def test_JSON_page_content(self):
        response = Client().get('/books/')
        html_response = response.content.decode('utf8')
        self.assertIsNotNone(html_response)
        self.assertIn('Books', html_response)