from django.urls import path
from . import views

urlpatterns = [
    path('',views.indexbooks,name='books'),
    path('readJSON/<str:query>/', views.readJSON, name='readJSON'),
]